Server basically a trim down of direwolf with FTBLite in mind. 
FTBLite means you can play this on your windows-tablets.
You can dual wield in game. You can have mystcraft without profiling. 
You can have Thaumcraft. You can have computer craft and turtle your way. 
Come and have fun. Server resets when people griefing and we vote for reset.

If you don't have git, download it here:
https://tortoisegit.org/download/
1. git clone https://craft558@bitbucket.org/craft558/mod_xxs.git
2. Install and make sure you are able to play FTBLite Lite 3 V1.5.0 using FTB_Launcher_1.4.3.jar in the folder.
   Don't worry if you can not login without mc account
3. put mods in FTBLite mods
4. extract mods/config.zip to FTBLite/minecraft/config and override
5. login to server: blue.rwind.tk
6. The server is offline server with no whitelist, which mean anyone can do anything on this server
7. you can register your account in game
/register <password> - Set your password
/rmpass - Removes your password
/login <password> - Login with your password
/changepass <old> <new> - change your password
/logout - Logout
8. Have a lot of fun!!!